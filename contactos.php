<?php
	
	class Contacto {
		public $nombre;
	  	public $apellidos;
	  	public $telefono;
	  	public $email;

	  	public function __construct($nombre, $apellidos, $telefono, $email) {
	    	$this->nombre = $nombre;
	    	$this->apellidos = $apellidos;
	    	$this->telefono = $telefono;
	    	$this->email = $email;
		}
	
		public function añadir($db) {
			try {
				$insert = "INSERT INTO contactos (nombre, 
												  apellidos, 
												  telefono, 
												  email) 
	                  	   			   VALUES (:nombre, 
	                  	   		   			   :apellidos, 
	                  	   		   			   :telefono, 
	                  	   		   			   :email)";
				
				$datos = array('nombre' => $this->nombre,
	                    'apellidos' => $this->apellidos,
	                    'telefono' => $this->telefono,
	                    'email' => $this->email);
						
				$stmt = $db->prepare($insert);
				
	      		$stmt->execute($datos);
				
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}
		
		public function modificar($db, $id) {
		    try {
			    $update = "UPDATE contactos SET nombre=:nombre, 
			      								apellidos=:apellidos, 
			      								telefono=:telefono, 
			      								email=:email 
			      							WHERE id=:id";
		      
			    $datos = array('nombre' => $this->nombre,
			                    'apellidos' => $this->apellidos,
			                    'telefono' => $this->telefono,
			                    'email' => $this->email,
			                    'id' => $id);
		
		      
		    	$stmt = $db->prepare($update);
		    	
		    	$stmt->execute($datos);
		
		    } catch(PDOException $e) {
		    	echo $e->getMessage();
		    }
		}
	}

?>