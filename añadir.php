<?php 

	include 'templates/header.php' 

?>

	<form id="añadir" action="añadir.php" method="POST">
		<label for="nombre">Nombre: </label>
		<input type="text" size=30 id="nombre" name="nombre" required>
		</br>
		<label for="apellidos">Apellidos: </label>
		<input type="text" size=30 id="apellidos" name="apellidos" required>
		</br>
		<label for="telefono">Teléfono: </label>
		<input type="text" size=30 id="telefono" name="telefono" maxlength="9" required>
		</br>
		<label for="email">Email: </label>
		<input type="email" size=30 id="email" name="email" required>
		</br>
		</br>
		<input type="submit" value="Enviar">
	</form>

<?php 

	include 'templates/footer.php' 

?>


<?php

	include 'db.php';
	include 'contactos.php';

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$nombre = $apellidos = $tlf = $email = "";	
		if (!empty($_POST["nombre"]) && !empty($_POST["apellidos"]) && !empty($_POST["telefono"]) && !empty($_POST["email"])) {
			$nombre = $_POST["nombre"];
			$apellidos = $_POST["apellidos"];
			$telefono = $_POST["telefono"];
			$email = $_POST["email"];
	
			$contacto = new Contacto($nombre, $apellidos, $telefono, $email);
			$contacto->añadir($db);
		}
	}

?>