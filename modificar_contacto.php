<?php

	include 'db.php';
	
	include 'contactos.php';
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$id = $nombre = $apellidos = $telefono = $email = "";
	  	if (!empty($_POST["nombre"]) && !empty($_POST["apellidos"]) && !empty($_POST["telefono"]) && !empty($_POST["email"])) {
	    	$id = $_POST["id"];
			$nombre = $_POST["nombre"];
			$apellidos = $_POST["apellidos"];
			$telefono = $_POST["telefono"];
			$email = $_POST["email"];
	
		    $contacto = new Contacto($nombre, $apellidos, $telefono, $email);
		    $contacto->modificar($db, $id);
	  	}
	}

	header('location: index.php');
	
?>