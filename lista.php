<?php

	include 'db.php';

	$lista = "";
	$tipo = "";

	// Creamos el query para listar el listado
 	if (isset($_GET['listar'])) {
    	$listar = $_GET['listar'];
		$lista = " ORDER BY LOWER(" . $listar . ")";
	}

	if (isset($_GET['tipo']) && $_GET['tipo'] == 'desc') {
		$tipo = " " . $_GET['tipo'];
	}

	$query = "SELECT * FROM contactos" . $lista . $tipo;
	$efecto = $db->query($query);

	include 'templates/header.php';
	
?>

	<div id="lista">
	<table>
		<tr>
	    	<th>
	    		<a href="?listar=nombre&tipo=desc">▼</a> Nombre <a href="?listar=nombre&tipo=asc">▲</a>
	    	</th>
	    	<th>
	    		<a href="?listar=apellidos&tipo=desc">▼</a> Apellidos <a href="?listar=apellidos&tipo=asc">▲</a>
	    	</th>
	    	<th>
	    		<a href="?listar=telefono&tipo=desc">▼</a> Teléfono <a href="?listar=telefono&tipo=asc">▲</a>
	    	</th>
	    	<th>
	    		<a href="?listar=email&tipo=desc">▼</a> email <a href="?listar=email&tipo=asc">▲</a>
	    	</th>
		</tr>
    
<?php

	foreach($efecto as $fila){
	    echo "<tr>";
	    echo "<td>" . $fila['nombre'] . "</td>";
	    echo "<td>" . $fila['apellidos'] . "</td>";
	    echo "<td>" . $fila['telefono'] . "</td>";
	    echo "<td>" . $fila['email'] . "</td>";
	    echo "</tr>";
	}
	
	echo "</table></div>";

	include 'templates/footer.php';
	
?>