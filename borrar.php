<?php

	include 'db.php';
	include 'templates/header.php';
	
	$efecto = $db->query('SELECT * FROM contactos');
	
?>

	<form id="borrar" action='borrar.php' method='POST'>
		<table>
			<tr>
	      		<th>Borrar</th>
	      		<th>Nombre</th>
	      		<th>Apellidos</th>
	      		<th>Teléfono</th>
	      		<th>Email</th>
	    	</tr>

<?php

	foreach($efecto as $fila) {
    	echo "<tr>";
	    echo "<td><input type='checkbox' name='contactos[]' value='" .  $fila['id'] . "'></td>";
	    echo "<td>" . $fila['nombre'] . "</td>";
	    echo "<td>" . $fila['apellidos'] . "</td>";
	    echo "<td>" . $fila['telefono'] . "</td>";
	    echo "<td>" . $fila['email'] . "</td>";
	    echo "</tr>";
  	}
  
	echo "</table>
	<input type='submit' value='Borrar'>
	</form>";

	include 'templates/footer.php';
	
?>



<?php

	include 'db.php';

	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['contactos'])) {
  		$contactos = $_POST['contactos'];

	  	if (!empty($contactos)) {
	    	for($i = 0; $i < count($contactos); $i++) {
	 		    $stmt = $db->prepare("DELETE FROM contactos WHERE id=:id");
	      		$stmt->bindParam(':id', $contactos[$i]);
	      		$stmt->execute();
	    	}
	  	}
	}
	
?>