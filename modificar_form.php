<?php

	include 'db.php';

	include 'contactos.php';
	
	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['contacto'])) {
		$stmt = $db->prepare('SELECT * FROM contactos WHERE id = :id');
  		$stmt->bindParam(':id', $_POST['contacto']);
 		$stmt->execute();

	 	foreach($stmt as $fila) {
			$contacto = new Contacto($fila['nombre'], 
									 $fila['apellidos'], 
									 $fila['telefono'], 
									 $fila['email']);
	 	}
	
	  	include 'templates/header.php';
		
?>

  	<form id="modificar2" action="modificar_contacto.php" method="POST" >
	    <input type="hidden" name="id" value="<?php echo $_POST['contacto'] ?>">
	    <label for="nombre">Nombre: </label>
		<input type="text" size=30 id="nombre" name="nombre" required value="<?php echo $contacto->nombre ?>">
		</br>
	    <label for="apellidos">Apellidos: </label>
		<input type="text" size=30 id="apellidos" name="apellidos" required value="<?php echo $contacto->apellidos ?>">
		</br>
		<label for="telefono">Teléfono: </label>
		<input type="text" size=30 id="telefono" name="telefono" maxlength="9" required value="<?php echo $contacto->telefono ?>">
	    </br>
		<label for="email">Email: </label>
		<input type="email" size=30 id="email" name="email" required value="<?php echo $contacto->email ?>">
		</br>
		</br>
	    <input type="submit" value="Enviar">
  	</form>
  

<?php
	include 'templates/footer.php'; 
	
	} else {
		header('location: index.php');
	}
?>